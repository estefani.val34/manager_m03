/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productmanagerapp;

import java.util.Scanner;

/**
 *
 * @author Alumne
 */
public class ProductManagerApp {

    Store myStore;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ProductManagerApp myApp = new ProductManagerApp();
        myApp.run();
    }

    /**
     * This method runs my application
     */
    private void run() {
        int option = 0;

        myStore = new Store(10);
        loadData();

        do {
            option = showMenu();
            switch (option) {
                case 1:
                    System.out.println("List all");
                    listAllProducts(myStore);

                    break;
                case 2:
                    System.out.println("Find by code");
                    findProductByCode(myStore);
                    break;
                case 3:
                    System.out.println("Find by name");
                    findProductByName(myStore);
                    break;
                case 4:
                    System.out.println("Add new");
                    addNewProduct(myStore);
                    break;
            }

        } while (option != 0);
    }

    /**
     * Shows a menu to the user and asks for an option
     *
     * @return the selected option or -1 in case of error
     */
    private int showMenu() {
        int option = -1;
        System.out.println("0.-Exit");
        System.out.println("1.-List All Products");
        System.out.println("2.-Find by code");
        System.out.println("3.-Find by name");
        System.out.println("4.-Add new");

        System.out.println("Choose your option: ");

        try {
            Scanner myScan = new Scanner(System.in);
            option = myScan.nextInt();
        } catch (Exception e) {
            System.out.println("Data error");
        }

        return option;
    }

    private void loadData() {
        Product p1 = new Product("kb1", "Keyboard", 20);
        myStore.add(p1);

        myStore.add(new Product("m2", "Monitor", 120));
        myStore.add(new Product("ms1", "Mouse", 15));
        myStore.add(new TV("tvSamsung", "Televisor", 200, 42));

        //2 productos de nevera
        myStore.add(new Nevera(3, "Ver21", "nevera1", 22.3));
        myStore.add(new Nevera(4, "Ver234", "nevera2", 212.4));

        //2 productos de ordenadores
        myStore.add(new Ordenador(12, 2, "Ord1213", "Ordenadr1", 22.3));
        myStore.add(new Ordenador(2, 4, "Ord234", "ordenadr2", 212.4));
    }

    

    private void listAllProducts(Store myStore) {

        for (int i = 0; i < myStore.getProducts().length; i++) {
            if (myStore.getProducts()[i] != null) {
                System.out.println(myStore.getProducts()[i]);
            }
        }
    }

    private void findProductByName(Store myStore) {
        Scanner teclat = new Scanner(System.in);

        System.out.println("Introduce el nombre del producto a buscar: ");
        String productNameBuscar = teclat.nextLine();

        for (int i = 0; i < myStore.getProducts().length; i++) {
            if (myStore.getProducts()[i] != null) {
                if (myStore.getProducts()[i].getName().equals(productNameBuscar)) {
                    System.out.println(myStore.getProducts()[i]);
                }

            }
        }
    }

    private void findProductByCode(Store myStore) {
         Scanner teclat = new Scanner(System.in);

        System.out.println("Introduce el codigo del producto a buscar: ");
        String productCodeBuscar = teclat.nextLine();

        for (int i = 0; i < myStore.getProducts().length; i++) {
            if (myStore.getProducts()[i] != null) {
                if (myStore.getProducts()[i].getCode().equals(productCodeBuscar)) {
                    System.out.println(myStore.getProducts()[i]);
                }
            }
        }
        
    }

    private void addNewProduct(Store myStore) {
         Scanner teclat = new Scanner(System.in);
        //String code, String name, double price
        System.out.println("Enter code of product:");
       // String 
        System.out.println("Enter name of code:");
        System.out.println("Enter price of product:");
    }
    
   
    

}
