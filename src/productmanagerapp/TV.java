/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productmanagerapp;

/**
 *
 * @author Alumne
 */
public class TV extends Product {
    private int inches; 
    
    //constructors
    public TV(String code, String name, double price, int inches){
        super (code,name,price);
        this.inches = inches;
    }
    public TV(String code){
        super(code);
    }
    public TV (TV other){
        super(other.code,other.name,other.price);
        this.inches = other.inches;
    }
    //setters getters

    public int getInches() {
        return inches;
    }

    public void setInches(int inches) {
        this.inches = inches;
    }
    
    @Override
    public String toString(){
        StringBuilder sb= new StringBuilder();
        sb.append("TV:{");
        sb.append("inches=");
        sb.append(inches);
        sb.append(super.toString());
        sb.append("}");
        
        return sb.toString();
    }
}
