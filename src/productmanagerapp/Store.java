/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productmanagerapp;

/**
 *
 * @author tarda
 */
public class Store {
    //Attributes
    private Product[] products;
    private int maxElements;
    private int numElements;
    
    //Constructors

    public Store(int maxElements) {
        this.maxElements = maxElements;
        this.numElements = 0;
        this.products = new Product[maxElements];
    }
    
    //setters i getters

    public Product[] getProducts() {
        return products;
    }

    public void setProducts(Product[] products) {
        this.products = products;
    }

    public int getMaxElements() {
        return maxElements;
    }

    public void setMaxElements(int maxElements) {
        this.maxElements = maxElements;
    }

    public int getNumElements() {
        return numElements;
    }

   /* public void setNumElements(int numElements) {
        this.numElements = numElements;
    }*/
    
    //Methods
    /**
     * Adds a product to the array of products if there is space
     * @param p
     * @return 0 if succes and -1 otherwise
     */
    public int add(Product p){
        int result = -1;
        if(numElements<maxElements){
            products[numElements]=p;
            numElements++;
            result = 0;
        }else result = -1;
        
        return result;
    }
    
    
    
}
