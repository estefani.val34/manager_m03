/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productmanagerapp;

import java.util.Objects;

/**
 *
 * @author Alumne
 */
public class Product {

    //Attributes
    String code;
    String name;
    double price;

    //Constructors
    /**
     * Constructor void
     */
    public Product() {
    }

    /**
     * Constructor with code
     *
     * @param code
     */
    public Product(String code) {
        this.code = code;
    }

    /**
     * Full constructor
     *
     * @param code
     * @param name
     * @param price
     */
    public Product(String code, String name, double price) {
        this.code = code;
        this.name = name;
        this.price = price;
    }

    //Setters and getters
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Product{code=");
        sb.append(code);
        sb.append(",name=");
        sb.append(name);
        sb.append(", price=");
        sb.append(price);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.code);
        return hash;
    }
    
    /**
     * Two products are equals if their code is the same.
     * @param obj
     * @return true if the code of the object (this) is equals to obj. False otherwise
     */
    @Override
    public boolean equals(Object obj){
        boolean b = false;
        if(obj==null) b=false;
        else{
            if(obj==this) b=true;
            else{
                if(obj instanceof Product){
                    Product other = (Product)obj;
                    b = this.code.equals(other.code);
                }else b=false;
            }
        }
        return b;
    }

}
