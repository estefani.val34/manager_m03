/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productmanagerapp;

/**
 *
 * @author tarda
 */
public class Ordenador extends Product {

    int ram;
    int hd;

    public Ordenador(int ram, int hd) {
        this.ram = ram;
        this.hd = hd;
    }

    public Ordenador(int ram, int hd, String code) {
        super(code);
        this.ram = ram;
        this.hd = hd;
    }

    public Ordenador(int ram, int hd, String code, String name, double price) {
        super(code, name, price);
        this.ram = ram;
        this.hd = hd;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public int getHd() {
        return hd;
    }

    public void setHd(int hd) {
        this.hd = hd;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        sb.append("Ordenador{");
        sb.append("ram=");
        sb.append(ram);
        sb.append(" ");
        sb.append(super.toString());
        sb.append("}");

        return sb.toString();

    }

}
