/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productmanagerapp;

/**
 *
 * @author tarda
 */
public class Nevera extends Product {

    private int numPortes;

    public Nevera(int numPortes) {
        this.numPortes = numPortes;
    }

    public Nevera(int numPortes, String code) {
        super(code);
        this.numPortes = numPortes;
    }

    public Nevera(int numPortes, String code, String name, double price) {
        super(code, name, price);
        this.numPortes = numPortes;
    }

    public int getNumPortes() {
        return numPortes;
    }

    public void setNumPortes(int numPortes) {
        this.numPortes = numPortes;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Nevera{");
        sb.append("numPortes=");
        sb.append(numPortes);
         sb.append(" ");
        sb.append(super.toString());
        sb.append("}");

        return sb.toString();

    }

}
